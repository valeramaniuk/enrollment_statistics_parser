import logging
import os
from datetime import datetime
from pdf_parser.parser_debug.output_to_txt import dump_to_txt

from pdf_parser.parser_logger import logger
from s3_helpers import s3_helper
from rds import update_db
from tsql import update_db as tsql_update_db
from . import pdf_to_text, get_lines,\
    pattern_search, get_datastructure,\
    get_time_of_publication
from processed_pdfs.dynamo import mark_file_as_processed


def pub_time_is_OK(datetime_obj):
    # we dont need files created after 19:00 or before 6:00
    # because of the but # CSUN there multiple files published 2016/10/11 10:00:22
    if (datetime_obj.hour > 19 or datetime_obj.hour <= 6) or\
            (datetime_obj.hour == 10
             and datetime_obj.minute == 0
             and datetime_obj.second == 22
             and datetime_obj.month == 11):
        logger.info("Rejected publication time {}:{}:{}"
                     .format(datetime_obj.hour, datetime_obj.minute, datetime_obj.second))
        return False
    else:
        return True


def main(next_file, term, default_db_engine, logging_mode=None, write_db=True):

    if logging_mode:
        logger.setLevel(logging.DEBUG)
        streamer = logging.StreamHandler()
        formatter = logging.Formatter('%(asctime)s - %(message)s')
        streamer.setFormatter(formatter)
        logger.addHandler(streamer)


    file_name = s3_helper.download_obj(next_file, term)
    logger.debug('PDF from S3 downloaded {}'.format(file_name))

    text_contents = pdf_to_text.pdf_to_txt(file_name)
    logger.debug('Text contents received {}'.format(next_file))

    time_of_publication = get_time_of_publication.get_time_of_publication(text_contents)
    str_time_of_publication = "{}-{}".format(
        time_of_publication['publication_date'],
        time_of_publication['publication_time']
        )
    datetime_obj_publication = datetime.strptime(str_time_of_publication, '%m/%d/%Y-%H:%M:%S')
    logger.debug('Time of publication of {} is {}'.format(next_file, datetime_obj_publication))

    # remove temp file
    os.remove(file_name)

    # if it's one of the "night" files - dont waste time on it
    if not pub_time_is_OK(datetime_obj_publication):
        mark_file_as_processed(next_file, 'bad')
        return

    # get the contets of the PDF as a list of lines
    list_of_lines = get_lines.get_lines(text_source=text_contents)
    logger.debug('List of lines received len ={}'.format(len(list_of_lines)))

    # get patterns from the list of lines
    list_of_patterns = pattern_search.pattern_search(list_of_lines)
    logger.debug('List of patterns received len ={}'.format(len(list_of_patterns)))

    # parse patterns in the lines to the data structures
    list_of_datastructures = get_datastructure.get_list_of_datastructures(list_of_patterns)
    logger.debug('List of datastructures received len ={}'.format(len(list_of_datastructures)))

    # if we are in debug mode - save all lines in the txt file for review
    # but there is no reason to modify the database - especially from the local machine
    #   it takes too long
    if logging_mode:
        dump_to_txt(list_of_lines, mode='a')
        logger.debug('Dumped to .txt')
    if write_db:
        if default_db_engine == 'postgres':
            update_db.update_db(list_of_datastructures, datetime_obj_publication, term)
            mark_file_as_processed(next_file, 'processed')
        if default_db_engine == 'tsql':
            tsql_update_db.update_db(list_of_datastructures, datetime_obj_publication, term)

    return
