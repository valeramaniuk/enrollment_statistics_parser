import PyPDF2

from pdf_parser.parser_logger import logger


def pdf_to_txt(filename):

        src = ""
        pdf_file_object = open(filename, 'rb')
        if pdf_file_object:
            pdf_reader = PyPDF2.PdfFileReader(pdf_file_object)


            for pageNum in range(0, pdf_reader.numPages):
                page_object = pdf_reader.getPage(pageNum)
                src += page_object.extractText()
            pdf_file_object.close()
            return src.lstrip()
        else:
            logger.error('ERROR: couldn\'t open PDF source file {}'.format(filename))
            return None
