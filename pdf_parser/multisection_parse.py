
def multi_section_parse(list_of_lines):
        # print_a_list(list_of_lines)

        '''If a line has only one COURSE ABBREVIATION
            but multiple 'Date:' entries - that means this course has multiple sections
            so we expand this sections like they are courses on their own
            returns a LIST of strings'''

        list_of_classes = []
        for src in list_of_lines:
            left_bound = src.find('Date')
            # print("LEFT BOUND:",left_bound)
            for i in range(src.count('Date') + 1):
                if i > 1:
                    right_bound = src.rfind('Date')
                    # print("RIGHT BOUND:", right_bound)
                    a = src[:left_bound] + src[right_bound:]
                    # print("Slice:", a)
                    src = src[:right_bound]
                    list_of_classes.append(a)
            list_of_classes.append(src)
        return list_of_classes