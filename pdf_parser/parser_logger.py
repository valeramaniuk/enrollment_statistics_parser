import logging
import os
from pathlib import Path


BASE_DIR = Path(__file__).parents[1]
filename = os.path.join(str(BASE_DIR), 'logs', 'parser.log')
filename_patterns = os.path.join(str(BASE_DIR), 'logs', 'failed_patterns.log')


logger = logging.getLogger('parsing')
logger_patterns = logging.getLogger('failed_patterns')

logger.setLevel(logging.INFO)
logger_patterns.setLevel(logging.DEBUG)


formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')

file_handler = logging.FileHandler(filename)
file_handler.setFormatter(formatter)
logger.addHandler(file_handler)


patterns_file_handler = logging.FileHandler(filename_patterns)
patterns_file_handler.setFormatter(formatter)
logger_patterns.addHandler(patterns_file_handler)


