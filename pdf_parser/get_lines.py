import re

from pdf_parser.parser_logger import logger
from .load_subjects import load_subjects
from .multisection_parse import multi_section_parse

def get_lines(text_source):
    set_of_subjects = load_subjects()
    text_source = cut_by_subject(text_source, set_of_subjects)
    list_of_lines = clean_service_lines(text_source)
    list_of_lines = glue_short_string_back(list_of_lines)
    list_of_lines = multi_section_parse(list_of_lines)
    return list_of_lines


def cut_by_subject(text_source, set_of_subjects):
        """there is a predefined list of course subjects
            and we assume that every time one of these subject appears in
            the text we need to parse - this is actually a new course.
            We add a newline before such "course subject", so it becomes actually a line.
            for example:
                COMP 456 blah-blah COMP 478 yada blach ART 123 blah... ->
                COMP 456 blah-blah
                COMP 478 yada... and so on
            this is not strictly necessary, but helps a lot to make the whole
            process easier to understand and work with.

            IMPORTANT: this assumption is actually incorrect, so we need a helper function
            to glue some (mistakenly cut) lines back
            """

        for subject in set_of_subjects:
            text_source = text_source.replace(subject, '\n' + subject.lstrip(' '))
            # print(subject)
        return text_source


def glue_short_string_back(list_of_lines):
        # print_a_list(list_of_lines)
        '''some strings were cut my mistake, so we check their length, and if its suspiciously short
            we glue it to the next string
            example ASS 110 Description <will be cut by mistake here> ASS Room: Date'''
        new_list_of_courses = []
        temp = ''

        for course_line in list_of_lines:

            course_line.strip('')
            course_line = temp + course_line
            if len(course_line) < 50:
                temp = course_line

            else:
                new_list_of_courses.append(course_line)
                temp = ''
        return new_list_of_courses

def clean_service_lines(text_source):
    """ need to identify and ignore metadata which appears on every page
        such as page number, date, notes and etc.
        :argument a raw text as
        :return list of "cleaned" lines """


    # one of these wors may appear in the service lines
    bad_words_pattern = re.compile(r'.*Classes|Northridge|NRPA\d{0,10}|'
                                   r'UndergraduateNote|Classes Open|Version: |'
                                   r'UndergraduateNote|CLASSES|Printed:.*')

    # These are not words, but dates, page numbers and etc
    # we don't want them neither

    bits_and_pieces_pattern = re.compile(r'Page|\sof\s|\d{1,10}\.\d{1,3}|'
                                         r'\d\d/\d\d/\d\d\d\d\s\d\d:\d\d:\d\d|'
                                         r'^\d{1,3}')

    text_source = text_source.replace('Program ID:', '\n')  # newline for the service string at the end of the page
    text_source = text_source.replace('Date:', ' Date:')  # add an extra space before the Date

    # creates a list out of a text file
    # every lines becomes an element of the list
    list_of_source_lines = text_source.split('\n')
    new_list_of_source_lines = []

    for line in list_of_source_lines:
        # check if this line contains any of the "bad" words
        # if it does - the line is a "service line" and needs to be ignored
        # and if it does not - append the line to the list of "good" lines for
        # further processing
        if not bad_words_pattern.search(line) and \
                not bits_and_pieces_pattern.search(line):
            new_list_of_source_lines.append(line)

    if new_list_of_source_lines[0] == '':
        new_list_of_source_lines.pop(0)  # HACK, the 0 element somehow was a ''
    return new_list_of_source_lines