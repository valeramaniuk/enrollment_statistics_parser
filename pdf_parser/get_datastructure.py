import datetime
import re

from pdf_parser.parser_logger import logger


def get_list_of_datastructures(list_of_patterns):

    list_of_datastructures = []
    for pattern in list_of_patterns:
        list_of_datastructures.append(get_datastructure(pattern))

    return list_of_datastructures


def time_parsing(time_string):
        '''Gets string like 01:30AM - 2:15PM
        and yields datetime.time'''

        pattern = re.compile(r'([0-9 :AMP]+)\s-\s([0-9 :AMP]+)')

        times = pattern.search(time_string)
        if times:
            time_start = times.group(1)
            time_end = times.group(2)
        else:
            time_start = time_end = None


        if time_start:
            try:
                time_start = time_start.strip()
                time_start = datetime.datetime.strptime(time_start, '%I:%M%p').time()
            except ValueError:
                time_start = datetime.datetime(1, 1, 1, 0, 0).time()
                logger.debug('TIME PARSING. time_start is undetermined for {}'.format(time_start))
        else:
            time_start = datetime.datetime(1, 1, 1, 0, 0).time()

        if time_end:
            try:
                time_end = time_end.strip()
                time_end = datetime.datetime.strptime(time_end, '%I:%M%p').time()
            except ValueError:
                time_end = datetime.datetime(1, 1, 1, 0, 0).time()
                logger.debug('TIME PARSING. time_end is undetermined for {}'.format(time_end))
        else:
            time_end = datetime.datetime(1, 1, 1, 0, 0).time()
        return time_start, time_end


def get_datastructure(mo):

    datastructure = dict()

    try:

        datastructure['course_subject'] = mo.group('course_subject')
        datastructure['course_level'] = mo.group('course_level')
        datastructure['course_title'] = mo.group('course_title').strip()
        datastructure['course_type'] = mo.group('course_type')
        datastructure['session'] = mo.group('session')
        datastructure['class_number'] = mo.group('class_number')
        datastructure['section_number'] = mo.group('section_number')[:2]
        datastructure['current_enrolment'] = mo.group('enrollment_available')
        datastructure['max_enrollment'] = mo.group('enrollment_max')
        datastructure['units'] = mo.group('units')
        datastructure['date_start'] = mo.group('date_start')
        datastructure['date_end'] = mo.group('date_end')
        datastructure['time'] = mo.group('time')
        datastructure['room'] = mo.group('room').strip()
        datastructure['instructor'] = mo.group('instructor')
        datastructure['days'] = mo.group('days')

        datastructure['days'] = datastructure['days']\
            .replace('Th', 'H')\
            .replace('Tu', 'T')\
            .replace('Sa', 'S')

    except AttributeError as e:
        logger.warning('FAILED to split re.object to groups {}'.format(e))


    # changes the format of the times and dates representation
    try:
        datastructure['date_end'] = datetime.datetime\
            .strptime(datastructure['date_end'], '%m/%d/%Y').date()
        datastructure['date_start'] = datetime.datetime\
            .strptime(datastructure['date_start'], '%m/%d/%Y').date()
        datastructure['time_start'], datastructure['time_end'] = time_parsing(datastructure['time'])
    except Exception as e:
        logger.error('FAILED to TIME/DATE format change {}'.format(e))

    # print(type(datastructure['time_end']), datastructure['time_end'])
    # print(type(datastructure['time_start']), datastructure['time_start'])
    #
    assert isinstance(datastructure['date_end'], datetime.date), 'date_end is not a datetime'
    assert isinstance(datastructure['date_start'], datetime.date), 'date_start is not a datetime'
    assert isinstance(datastructure['time_end'], datetime.time), 'time_end is not a datetime'
    assert isinstance(datastructure['time_start'], datetime.time), 'time_start is not a datetime'

    return datastructure
