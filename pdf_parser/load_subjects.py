import os
from pathlib import Path
from pdf_parser.parser_logger import logger


def load_subjects():

        """takes a txt file of course subjects, cleans a bit
        and returns a set of course subjects"""
        # gets the parent directory of the project
        # by moving two directories up
        BASE_DIR = Path(__file__).parents[1]

        filename = os.path.join(str(BASE_DIR), 'data', 'course_subjects.txt')
        set_of_subjects = []
        with open(filename, 'r') as f:
            if f:
                for line in f:
                    if not line.startswith('#'):
                        set_of_subjects.append(line.strip('\"\',\n') + '  ')
            else:
                logger.error('ERROR: couldn\'t open Subject txt File')
        return set_of_subjects

