import re
from pdf_parser.parser_logger import logger


def get_time_of_publication(text):

    try:
        pattern = re.compile(r'(Printed:\n(\d\d/\d\d/\d\d\d\d) (\d\d:\d\d:\d\d))')
        times = pattern.search(text)

        return {'publication_date': times.group(2),
                'publication_time': times.group(3)
                }

    except Exception as e:
            logger.error("No publication date pattern found in {} with error {}"
                         .format(text, e))

