import re

from pdf_parser.parser_logger import logger, logger_patterns


def pattern_search(list_of_lines):
    """ goes througth all lines one by one and tries to match
     predefined a pattern. If there is a match - > yields the
      If there is no match - complain about it, since the assumtion is:
      at this point all lines we are working with contain info about courses

    """

    list_of_patterns = []

    pattern = re.compile(r'^\s*(?P<course_subject>\w+\s?\w+)\s+'
                         r'(?P<course_level>\d{1,3}[A-Z]*?)\s+'
                         r'(?P<course_title>[0-9A-Z\s!@#$^&*,.()/&-+:+~`\-?\'\"]+)\s*'
                         r'(?P<session>Regular)\s+'
                         r'(?P<section_number>\d\d\w?\d?)\s+'
                         r'(?P<course_type>\w{3,10})\s+'
                         r'(?P<class_number>\d{3,6})\s+'
                         r'(?P<enrollment_available>\d{0,4})\s+'
                         r'(?P<enrollment_max>\d{0,4})'
                         r'\s+(?P<units>\d\.?\d?\d?)\s+'
                         r'Date:\s+(?P<date_start>[0-9/]+)\s+-\s+'
                         r'(?P<date_end>[0-9/]+)\s+'
                         r'Time:\s+(?P<time>[0-9AMP :-]+)\s'
                         r'Days:\s+(?P<days>[A-Za-z]+)\s+'
                         r'Room:\s+(?P<room>[A-Za-z0-9@&\- ]+)\s+'
                         r'Instructor:\s+(?P<instructor>[A-Za-z, ]+)')

    for line in list_of_lines:
        mo = None
        try:
            mo = pattern.search(line.strip())
        except TypeError as e:
            logger_patterns.debug('Line contained some characters that re.search failed {}\n '
                         .format(e))

        if mo:
            # print(mo.group(0))
            list_of_patterns.append(mo)
        else:
            logger_patterns.debug('FAILED to find PATTERN in the\n {}'.format(line))
    return list_of_patterns
