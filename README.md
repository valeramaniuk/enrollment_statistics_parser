# Enrollment statistics for CSUN Sping 2017 #
http://csun.ninja

### Idea ###

Every hour during the class registration season the University publishes the PDF file with the list of all classes CSUN has to offer in the upcoming semester @ http://csun.edu/openclasses.
Along with some general information about the class, current and maximum enrollment numbers for every class are available. This data needs to be collected, stored and aggregated.
The separate application then displays/visualizes the enrollment data https://bitbucket.org/valeramaniuk/enrollment_statistics_frontend .

### Why is it useful ####

The information about enrollment dynamics is not publicly available, and there is no way for a student with a relatively late registration date to predict which courses he/she will be able to add. Therefore no planning is possible.
By comparing historical and current data, some meaningful insights might be made.

### Architecture ###

* AWS Lambda function (python) on schedule saves the newly published PDFs to S3
* The list of already processed PDFs is maintained in the DynamoDB table
* The list of PDFs to parse is stored in AWS SQS, for distributed parsing
* Fleet of EC2 polling the queue, retrieves files from S3 and parses PDFs
* Parsed data is being stored to in one of the AWS RDS servers


### To Do ###

* Tests
* Tune logging levels

### Usage ###
* **--term <term>** specifies pdfs for whicj term to parse, "spring2017" for Spring, default is Fall 2017
* **--in-place** parses only the hardcoded list of objects, for debugging purposes
* **--no-write-db** do not write to DB (useful with --in-place)
* **--threads <num>** set the number of parsing threads for the worker
* **--forget-processed** flushes the DynamoDB table, which holds the names of already processed files
* **--populate-queue** flushes the SQS and repopulates it with names of all files in the S3 bucket (PDFs)
* **--db-engine <tsql/postres>** selects the database and server (as of now there are 2 databases)
### Contact ###

* Valera MANIUK valery.maniuk.55@my.csun.edu


![](https://s3-us-west-1.amazonaws.com/csun.ninja.pics/cloudcraft+-+csun.ninja+arch+1.png)