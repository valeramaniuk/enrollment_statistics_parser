import boto3
import os
from pathlib import Path
from .s3_logger import logger

BUCKET_NAME_SPRING = 'csunpdfspring2017'
BUCKET_NAME_FALL = 'csunpdffall2017'

def get_list_of_objects(term):
    # returns a list of filenames stored in S3  bucket BUCKET_NAME
    s3 = boto3.resource('s3')
    if term == 'spring2017':
        BUCKET_NAME = BUCKET_NAME_SPRING
    else:
        BUCKET_NAME = BUCKET_NAME_FALL
    bucket = s3.Bucket(BUCKET_NAME)
    list_of_objects = []
    for key in bucket.objects.all():
       list_of_objects.append(key.key)

    return list_of_objects


def download_obj(nextfile, term):
    # returns the contents of the object
    # with the key: nextfile from the bucket BUCKET_NAME

    BASE_DIR = Path(__file__).parents[0]
    temp_pdf_save_path = os.path.join(str(BASE_DIR), 'temp_pdf', nextfile)
    if term == 'spring2017':
        BUCKET_NAME = BUCKET_NAME_SPRING
    else:
        BUCKET_NAME = BUCKET_NAME_FALL
    # temp_pdf_save_path = os.path.join(os.path.dirname(os.path.abspath(__file__)), '../temp_pdf', nextfile)
    s3 = boto3.resource('s3')
    try:
        print("trying download {} {}".format(BUCKET_NAME, nextfile))
        s3.Object(BUCKET_NAME, nextfile).download_file(temp_pdf_save_path)
        logger.info("{} downloaded".format(nextfile))
        return temp_pdf_save_path
    except Exception as e:
        logger.error("Couldn\'t download object {} {}".format(nextfile, e))
        return None
