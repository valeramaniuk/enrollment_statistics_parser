import logging
import os
from pathlib import Path


BASE_DIR = Path(__file__).parents[1]
filename = os.path.join(str(BASE_DIR), 'logs', 's3.log')


logger = logging.getLogger('s3')

logger.setLevel(logging.DEBUG)

formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')


file_handler = logging.FileHandler(filename)
file_handler.setFormatter(formatter)

streamer = logging.StreamHandler()

logger.addHandler(file_handler)
logger.addHandler(streamer)

