import pypyodbc
# from .tsql_logger import logger

def update_db(list_of_data_structures, time_of_publication, term):
    SQL_proc = """EXECUTE sp_insert_data
             @term                   =?
            ,@course_level           =?
            ,@course_subject         =?
            ,@course_title           =?
            ,@course_type            =?
            ,@course_units           =?
            ,@class_number           =?
            ,@section_number         =?
            ,@section_max_enrollment =?
            ,@open_seats             =?
            ,@instructor             =?
            ,@room                   =?
            ,@days_list              =?
            ,@time_start             =?
            ,@time_end               =?
            ,@date_start             =?
            ,@date_end               =?
            ,@publication_time       =?

            """
    connection = connect()
    if connection:
        cursor = connection.cursor()
        values = []
        for i, datastructure in enumerate(list_of_data_structures):

            values.append ([
                term,
                datastructure['course_level'],
                datastructure['course_subject'],
                datastructure['course_title'],
                datastructure['course_type'],
                datastructure['units'],
                datastructure['class_number'],
                datastructure['section_number'],
                datastructure['max_enrollment'],
                datastructure['current_enrolment'],
                datastructure['instructor'],
                datastructure['room'],
                datastructure['days'],
                datastructure['time_start'],
                datastructure['time_end'],
                datastructure['date_start'],
                datastructure['date_end'],
                time_of_publication
            ])

            if i % 150 == 0:
                print ('added {} records'.format(str(i)))
                cursor.executemany(SQL_proc, values)
                connection.commit()
                values = []
        connection.close()



def connect():
    # connects to a specified database
    # and returns the connection object
    db_name = 'enrollment_stats'

    try:

        conn = pypyodbc.connect('Driver={SQL Server};'
                                'Server=tsql.ci0ug5lyinwr.us-west-2.rds.amazonaws.com;'
                                'Database=enrollment_stat;'
                                'uid=valeramaniuk;pwd=defectivebear')

        print('Connection to {} was SUCCESSFUL'.format(db_name))
        return conn
    except Exception as e:
        print('Unable to connect to database: {}, error: '.format(db_name, e))
        return None

# print (update_db())