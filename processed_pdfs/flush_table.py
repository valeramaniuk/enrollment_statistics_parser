import boto3
import time
import random
from .dynamo_logger import logger
from .dynamo_conf import *

dynamodb = boto3.resource('dynamodb', region_name = 'us-west-2' )
processed_table = dynamodb.Table('processed_pdfs')


def erase_processed_pdfs_table():
    logger.info('Flushing  processsed pdf table')
    scan = processed_table.scan(AttributesToGet=['filename',])

    for filename in scan['Items']:
        try:
            response =  processed_table.delete_item(
                    Key={
                        'filename': filename['filename'],
                    }
            )
            time.sleep(DELETE_DELAY)
        except:
            logger.exception("Unable to delete filename {} from processed_pdf table"
                             .format(filename['filename']))


    while 'LastEvaluatedKey' in scan:
        scan = processed_table.scan(AttributesToGet=['filename', ],
                                    ExclusiveStartKey=scan['LastEvaluatedKey'])
        try:
            for filename in scan['Items']:
                response = processed_table.delete_item(
                    Key={
                        'filename': filename['filename'],
                    }
                )

                logger.debug("Next key for the scan is {}".format( scan['LastEvaluatedKey']))
                time.sleep(DELETE_DELAY)
        except:
            logger.exception("Unable to delete filename {} from processed_pdf table"
                             .format(filename['filename']))

    return