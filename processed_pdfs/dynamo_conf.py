# delay between consequitive deletes
# when flushing the table
#
DELETE_DELAY = 0.15


# write delay
# random time delay when updating a table
# in hope to spread write load more evenly in case
# when many thread hit a streak of "bad time" pdfs

WRITE_DELAY = 2