import boto3
import random
import time

from .dynamo_logger import logger
from .dynamo_conf import *

dynamodb = boto3.resource('dynamodb', region_name='us-west-2')
processed_table = dynamodb.Table('processed_pdfs')
configuration_table = dynamodb.Table('configuration')


def check_if_file_was_processed(filename):
    # There is a list of files that were processed
    # (loaded into Dynamo)
    # Get the filename and compare it against this list
    response = processed_table.get_item(
        Key={
            'filename': filename
        }
    )
    try:
        if response['Item']['reason']:
            return True
        else:
            return False
    except KeyError:
        return False


def mark_file_as_processed(filename, reason):
    # add a filename to a list of PDF parsed and added to Dynamo

    # sleeps for a random ammount of time
    # before marks file as processed
    # in hope to even out the load
    time.sleep(random.randint(1, 1+WRITE_DELAY))

    processed_table.update_item(
        Key={
            'filename': filename,

        },
        UpdateExpression="set reason = :r",
        ExpressionAttributeValues={
            ':r': reason,

        },
        ReturnValues="UPDATED_NEW"
        )
    return


def get_db_password(db_name):
    logger.info('Getting the password for {}'.format(db_name))
    response = configuration_table.get_item(
        Key={
            'keys': db_name+"_conf"
        }
    )
    try:
        if response['Item']['values']:
            logger.info('Getting the password for {} SUCCESSFUL'.format(db_name))
            return response['Item']['values']
        else:
            logger.critical('Getting the password for {} FAILED'.format(db_name))
            return False
    except KeyError:
        logger.critical('Getting the password for {} FAILED'.format(db_name))
        return False
