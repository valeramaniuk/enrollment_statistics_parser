import psycopg2
import logging


def connect():
    # connects to a specified database
    # and returns the connection object
    db_name = 'zippo'
    try:
        conn = psycopg2.connect(
                host='postgredb.ci0ug5lyinwr.us-west-2.rds.amazonaws.com',
                port='5432',
                user='valeramaniuk',
                password='negative1988',
                dbname=db_name)

        return conn
    except Exception as e:
        logger.critical('Unable to connect to database: {}, error: '.format(db_name, e))
        return None