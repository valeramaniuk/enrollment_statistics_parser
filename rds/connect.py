import psycopg2
from .rds_logger import logger


from processed_pdfs.dynamo import get_db_password

def connect():

    # connects to a specified database
    # and returns the connection object
    db_name = 'enrollment_stats_new'
    CONF = get_db_password(db_name)

    try:

        conn = psycopg2.connect(
                host=CONF['host'],
                port=int(CONF['port']),
                user=CONF['username'],
                password=CONF['password'],
                dbname=db_name)

        logger.info('Connection to {} was SUCCESSFUL'.format(db_name))
        return conn
    except Exception as e:
        logger.critical('Unable to connect to database: {}, error: '.format(db_name, e))
        return None
