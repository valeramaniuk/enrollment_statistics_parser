import psycopg2
from .rds_logger import logger
from .connect import connect
SECTION_TABLE = ""
COURSE_TABLE = ""
SECTIONSCHEDULE_TABLE = ""
ENROLLMENT_TABLE = ""

def update_db(list_of_data_structures, time_of_publication, term='fall2017'):

    connection = connect()
    connection.autocommit = True

    global SECTION_TABLE
    global COURSE_TABLE
    global SECTIONSCHEDULE_TABLE
    global ENROLLMENT_TABLE

    FALL2017_SECTION_TABLE = 'display_stats_sectionfall2017'
    FALL2017_COURSE_TABLE = 'display_stats_coursefall2017'
    FALL2017_SECTIONSCHEDULE_TABLE = 'display_stats_sectionschedulefall2017'
    FALL2017_ENROLLMENT_TABLE = 'display_stats_enrollmentfall2017'

    SPRING2017_SECTION_TABLE = 'display_stats_sectionspring2017'
    SPRING2017_COURSE_TABLE = 'display_stats_coursespring2017'
    SPRING2017_SECTIONSCHEDULE_TABLE = 'display_stats_sectionschedulespring2017'
    SPRING2017_ENROLLMENT_TABLE = 'display_stats_enrollmentspring2017'


    if term == 'spring2017':
        SECTION_TABLE = SPRING2017_SECTION_TABLE
        COURSE_TABLE = SPRING2017_COURSE_TABLE
        SECTIONSCHEDULE_TABLE = SPRING2017_SECTIONSCHEDULE_TABLE
        ENROLLMENT_TABLE = SPRING2017_ENROLLMENT_TABLE

    # default if fall2017
    else:
        SECTION_TABLE = FALL2017_SECTION_TABLE
        COURSE_TABLE = FALL2017_COURSE_TABLE
        SECTIONSCHEDULE_TABLE = FALL2017_SECTIONSCHEDULE_TABLE
        ENROLLMENT_TABLE = FALL2017_ENROLLMENT_TABLE

    logger.debug("TABLE VARIABLES: {} {} {} {} ".format(SECTION_TABLE, COURSE_TABLE, SECTIONSCHEDULE_TABLE, ENROLLMENT_TABLE))


    for datastructure in list_of_data_structures:

        course_id = create_course(
                    datastructure=datastructure,
                    connection=connection,
                    )

        logger.debug('Course id  {}'.format(str(course_id)))

        if course_id:

            section_exists = create_section(
                                course_id=course_id,
                                datastructure=datastructure,
                                connection=connection,
                                publication_time=time_of_publication,
                                )
            if section_exists:

                create_enrollment(
                    datastructure=datastructure,
                    time_of_publication=time_of_publication,
                    connection=connection,
                    )

                create_section_schedule(
                    datastructure=datastructure,
                    time_of_publication=time_of_publication,
                    connection=connection,
                    )

    connection.close()
    return


def create_section_schedule(datastructure, time_of_publication, connection):
    cur = connection.cursor()


    sql = """
            BEGIN;
            INSERT INTO {schedule} (
                  class_number_id,
                  room,
                  instructor,
                  days,
                  time_start,
                  time_end,
                  date_start,
                  date_end,
                  publication_date
            )
            VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s)
                ON CONFLICT (class_number_id, time_start, time_end ) DO
            UPDATE
            SET
                  room = %s,
                  instructor = %s,
                  days = %s,
                  time_start = %s,
                  time_end = %s,
                  date_start = %s,
                  date_end = %s,
                  class_number_id = %s,
                  publication_date = %s
            WHERE
                    {schedule}.class_number_id = %s
                AND
                    {schedule}.time_start =%s
                AND
                    {schedule}.time_end =%s
                AND
                    {schedule}.publication_date < %s;
            COMMIT;
    """.format(schedule=SECTIONSCHEDULE_TABLE)



    instructor = datastructure['instructor']
    room = datastructure['room']
    days = datastructure['days']
    time_start = datastructure['time_start']
    time_end = datastructure['time_end']
    date_start = datastructure['date_start']
    date_end = datastructure['date_end']
    class_number = datastructure['class_number']

    if time_start == '':
        time_end = time_start = None

    try:
        logger.debug("Cretating SCHEDULE for {}".format(class_number))


        sql_args = (
            class_number,
            room,
            instructor,
            days,
            time_start,
            time_end,
            date_start,
            date_end,
            time_of_publication,
            room,
            instructor,
            days,
            time_start,
            time_end,
            date_start,
            date_end,
            class_number,
            time_of_publication,
            class_number,
            time_start,
            time_end,
            time_of_publication)

        cur.execute(sql, sql_args)
        cur.close()
        return True

    except psycopg2.IntegrityError as e:
        logger.exception("Failed create schedule {} {} , . Error: {}"
                         .format(class_number, time_of_publication, e))
        return False


def create_enrollment(datastructure, time_of_publication, connection):

    class_number = datastructure['class_number'],
    open_seats = datastructure['current_enrolment'],

    cur = connection.cursor()

    sql_insert = """
              INSERT INTO {enrollment}
                (
                    class_number_id,
                    open_seats,
                    publication_time)
              VALUES (%s, %s, %s)
              ON CONFLICT (class_number_id, publication_time) DO NOTHING;
            """.format(enrollment=ENROLLMENT_TABLE)

    try:
        cur.execute(sql_insert, (class_number, open_seats, time_of_publication))

    except psycopg2.IntegrityError as e:
        logger.exception("Failed to create enrollment data for {}  {}. Error: {}"
                         .format(class_number, time_of_publication,  e))

    cur.close()
    return


def create_section(datastructure, course_id, connection,publication_time):

    class_number = datastructure['class_number']
    section_number = datastructure['section_number']
    section_max_enrollment = datastructure['max_enrollment']

    cur = connection.cursor()

    sql = """

                INSERT INTO {section}
                ( class_number,
                  section_number,
                  section_max_enrollment,
                  course_id,
                  publication_time)

                VALUES (%s, %s, %s, %s, %s)
                    ON CONFLICT (class_number) DO NOTHING

            """.format(section=SECTION_TABLE)

    sql_args = (class_number,
                section_number,
                section_max_enrollment,
                course_id,
                publication_time
                )

    sql_exists ="""SELECT 1 FROM {section} WHERE class_number = %s"""\
                .format(section=SECTION_TABLE)


    try:
        logger.debug("Trying to create class number {}...".format(class_number))
        # cur.execute(sql_insert, (class_number, section_number, section_max_enrollment, course_id))
        cur.execute(sql, sql_args)
        connection.commit()
        logger.debug("Success class number {}...".format(class_number))

        # Checking if this section exists in the table
        # either after the INSERT or it was there before
        cur.execute(sql_exists, (class_number,))
        section_exists = cur.fetchone()
        if section_exists:
            return True
        else:
            return False

    except Exception as e:
        logger.exception("Failed insert section {}  {}".format(class_number, e))
        cur.close()
        return False


def create_course(datastructure, connection):

    # Tries to insert the new course and get its ID on success
    # if such course exists, then it won't get the ID
    # and has to query for it a separate query
    #
    # Returns an ID or
    # None on failure

    subject = datastructure['course_subject']
    level = datastructure['course_level']
    title = datastructure['course_title']
    course_type = datastructure['course_type']
    units = datastructure['units']

    cur = connection.cursor()
    # TODO: probably possible to get id on the same query, see comments below
    # RETURNING (id) doesnt work with ON CONFLICT
    # since it returns None in this case
    sql_insert = """
      INSERT INTO {course}
        ( course_subject,
          course_level,
          course_title,
          course_type,
          course_units
          )
      VALUES (%s, %s, %s, %s, %s)

      ON CONFLICT DO NOTHING

    """.format(course=COURSE_TABLE)


    sql_course_id = """

                        SELECT (id)
                        FROM {course}
                        WHERE
                              course_subject = %s
                              AND
                              course_level = %s


                        """.format(course=COURSE_TABLE)

    try:
        # try to insert
        cur.execute(sql_insert, (subject, level, title, course_type, units))
    except Exception as e:
        logger.exception('Unable to create course  {} {}, error: {}'
                         .format(datastructure['course_subject'],
                                 datastructure['course_level'], e
                                 )
                         )

    try:
        # gets the id of this course
        cur.execute(sql_course_id, (subject, level))
        course_id = cur.fetchone()
        if course_id:
            cur.close()
            return course_id[0]
        else:
            cur.close()
            return None
    except Exception as e:
        logger.exception('Unable to retrieve ID for course  {} {}, error: {}'
                         .format(datastructure['course_subject'],
                                 datastructure['course_level'], e
                                 )
                         )

    cur.close()
    return None
