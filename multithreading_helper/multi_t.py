import threading
import time

from pdf_parser import main_parser
from .mutli_logger import logger
from sqs import populate_queue

def thread_watchdog(_THREADS_COUNT, term, default_db_engine):
    #TODO: WRITE COMMENTS AND REFACTOR

    # hope that if I spread out the creation of the threads
    # they they won't hit the DB all it once
    _DELAY_BETWEEN_CREATION = 20
    exit_flag = 0

    class myThread (threading.Thread):
        def __init__(self, threadID, name):
            threading.Thread.__init__(self)
            self.threadID = threadID
            self.name = name

        def run(self):
            logger.info("Starting " + self.name)
            process_data(self.name, term)
            logger.info("Exiting " + self.name)

    def process_data(threadname, term):

        # to gracefully terminate threads
        # not implemented yet
        while not exit_flag:

            msg = populate_queue.get_next_filename_from_sqs()
            if msg:
                logger.debug("{} processing {}".format(threadname, msg.body))
                main_parser.main(msg.body, term, default_db_engine)
                logger.info("{} processed {} and is deleting the message".format(threadname, msg.body))
                msg.delete()

            else:
                logger.info("Queue {} is empty {} is wrapping up...".format(term, threadname))
                break



    threadlist = []
    for t in range(0, _THREADS_COUNT):
        threadlist.append("Thread-"+str(t))

    threads = []
    thread_id = 1

    # Create new threads
    for tName in threadlist:
        thread = myThread(thread_id, tName)
        thread.start()
        threads.append(thread)
        thread_id += 1
        time.sleep(_DELAY_BETWEEN_CREATION)

    # Wait for all threads to complete
    for t in threads:
        t.join()
    logger.info("Exiting Main Thread")
