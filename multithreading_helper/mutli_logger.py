import logging
import os
from pathlib import Path


BASE_DIR = Path(__file__).parents[1]
filename = os.path.join(str(BASE_DIR), 'logs', 'multi.log')

logger = logging.getLogger('multithreading')
logger.setLevel(logging.INFO)

formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')

file_handler = logging.FileHandler(filename)
file_handler.setFormatter(formatter)
logger.addHandler(file_handler)

streamer = logging.StreamHandler()

streamer.setFormatter(formatter)
logger.addHandler(streamer)
