import getopt
import sys

from multithreading_helper import multi_t
from pdf_parser import main_parser
from processed_pdfs import flush_table
from s3_helpers import s3_helper
from sqs import populate_queue





def main(argv):
    #TODO: do the proper logger

    '''
        options:
            --in-place
                parses only hardcoded list of files
                for debugging purposes
                utilizes 1 thread only
                disregards all other options
            --threads <int>
                set the number of threads
            --forget-processed
                flushes the Dynamo table containing filenames
                that were processed (one way or another)
            --populate-queue
                flushes and repopulates the Queue containing all the filenames
                for objects to process (PDFs) from S3 bucket
            --term <term>
                specifies pdfs for which term to parse, "spring2017" for Spring, default is Fall 2017
            --no-write-db
                do not write to DB (useful with --in-place)
            --db-engine <name of the engine> which engine to use and therefore to which database
                to connect

    '''

    _THREADS_COUNT = 1
    repopulate_queue = False
    forget_processed = False
    in_place = False
    write_db = True
    term = 'fall2017'
    list_of_db_engines = ['postgres', 'tsql']
    default_db_engine = 'postgres'
    try:
        opts, args = getopt.getopt(argv, "", ["threads=",
                                              "term=",
                                              "populate-queue",
                                              "forget-processed",
                                              "in-place",
                                              "no-write-db",
                                              "db-engine="])

    except getopt.GetoptError:
        print('launch.py --threads <int>'
              '\n\t --term <spring2017>'
              '\n\t --forget-processed'
              '\n\t --populate-queue'
              '\n\t --in-place'
              '\n\t--no-write-db'
              '\n\t--db-engine <postgres/tsql>'
              )
        sys.exit(2)

    for opt, arg in opts:

        # chose the term for which to parse schedules
        # the default is fall2017 which is the latest term at the moment
        if opt =='--term':
            if arg == 'spring2017':
                term = arg

        # choose a bd engine from the list
        # if it's not in the list - exit with the warning
        # if not specified - the default engine is used
        if opt =='--db-engine':
            if arg in list_of_db_engines :
                if arg != default_db_engine:
                    default_db_engine = arg
            else:
                print('Unknown DB Engine')
                exit()
        if opt == '--in-place':
            in_place = True

        if opt == '--no-write-db':
            write_db = False


        if opt == '--threads':
            if int(arg) > 0:
                _THREADS_COUNT = int(arg)
                print('Thread Count is set to {}'.format(arg))
            else:
                print("Thread count should be an integer")
                sys.exit(2)

        if opt == '--populate-queue':
            repopulate_queue = True

        if opt == '--forget-processed':
            forget_processed = True

    # option setting to variables complete

    # DEBUG mode
    if in_place:
        if term == 'spring2017':
            next_files = ['open_classes_spring_2017_1478820241.pdf',]
        else:
            next_files = ['open_classes_fall_2017_1494354237.pdf',]
        for next_file in next_files:
            main_parser.main(next_file,
                             term,
                             logging_mode='verbose',
                             write_db=write_db,
                             default_db_engine=default_db_engine)

    else:
        # production mode
        if forget_processed:
            print('Flushing processed pdf table')
            flush_table.erase_processed_pdfs_table()

        if repopulate_queue:
            print('Repopulating SQS...')
            if populate_queue.populate_queue(term):
                print('SQS repopulation finished {}'.format(term))
            else:
                print('SQS repopulation have failed{}'.format(term))
                sys.exit(2)

        multi_t.thread_watchdog(_THREADS_COUNT, term, default_db_engine)

    return

if __name__ == '__main__':
    main(sys.argv[1:])
