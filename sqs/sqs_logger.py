import logging
import os
from pathlib import Path

logging.getLogger('boto3').setLevel(logging.CRITICAL)
logging.getLogger('botocore').setLevel(logging.CRITICAL)
logging.getLogger('nose').setLevel(logging.CRITICAL)
logging.getLogger('s3transfer').setLevel(logging.CRITICAL)


BASE_DIR = Path(__file__).parents[1]
filename = os.path.join(str(BASE_DIR), 'logs', 'sqs.log')

logger = logging.getLogger('sqs')
logger.setLevel(logging.INFO)

formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')

file_handler = logging.FileHandler(filename)
file_handler.setFormatter(formatter)
logger.addHandler(file_handler)
streamer = logging.StreamHandler()
logger.addHandler(streamer)
