import logging
import boto3

from processed_pdfs.dynamo import check_if_file_was_processed
from s3_helpers.s3_helper import get_list_of_objects
from .sqs_logger import logger

S3_FILENAMES_QUEUE_NAME = 'filename_queue'
sqs = boto3.resource('sqs', region_name='us-west-2')


def populate_queue(term):
    logger.info("Preparing the SQS... ")
    list_of_files_in_the_bucket = get_list_of_objects(term)
    try:
        filename_queue = sqs.create_queue(
            QueueName=S3_FILENAMES_QUEUE_NAME,
            Attributes={
                          "ReceiveMessageWaitTimeSeconds": "20",
                          "VisibilityTimeout": "1800",
                      }
                      )

        logger.info('Purging the queue ...')
        filename_queue.purge()
        logger.info('Purging DONE.\nAdding messages to the Queue, %'.format(term))
    except:
        logger.exception('SQS Creation or Purging have failed, %s'.format(term))
        return None

    number_of_messages = 0
    for filename in list_of_files_in_the_bucket:
        if check_if_file_was_processed(filename):
            logger.info("{} was already processed, moving on...".format(filename))
            continue
        else:
            logger.info("Adding to queue {}".format(filename))


            number_of_messages+=1
            response = filename_queue.send_message(
                MessageBody=filename,
            )
    logger.info("{} messages added to filename queue".format(str(number_of_messages)))
    return number_of_messages

def get_next_filename_from_sqs():

    queue = sqs.get_queue_by_name(QueueName=S3_FILENAMES_QUEUE_NAME)
    r_handle = queue.receive_messages(MaxNumberOfMessages=1, AttributeNames=['All', ])
    if r_handle:
        return r_handle[0]
    else:
        return None